package com.swe.challenge.martin.LunchApplication.utils;

import java.util.UUID;

public class UuidGenerator {

    public static String generateUUID() {
        return UUID.randomUUID().toString().replace("-", "");
    }
}
