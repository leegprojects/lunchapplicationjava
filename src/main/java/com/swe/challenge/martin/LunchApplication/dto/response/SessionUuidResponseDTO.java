package com.swe.challenge.martin.LunchApplication.dto.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SessionUuidResponseDTO {
    private Long sessionId;
    private String sessionUuid;
}
