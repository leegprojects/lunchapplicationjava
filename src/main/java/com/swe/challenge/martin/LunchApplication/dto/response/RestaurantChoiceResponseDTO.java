package com.swe.challenge.martin.LunchApplication.dto.response;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Builder
public class RestaurantChoiceResponseDTO {

    private Long id;

    private String createdBy;

    private String restaurantName;

    private Long sessionId;
}
