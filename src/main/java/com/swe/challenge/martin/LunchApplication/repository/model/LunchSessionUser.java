package com.swe.challenge.martin.LunchApplication.repository.model;

import com.swe.challenge.martin.LunchApplication.constants.SessionStatus;
import jakarta.persistence.*;
import lombok.*;

import java.time.LocalDateTime;
import java.util.List;

@Entity
@Getter
@Setter
@Builder
@Table(name = "lunch_session_user")
@NoArgsConstructor
@AllArgsConstructor
public class LunchSessionUser {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long userId;

    @ManyToOne
    @JoinColumn(name = "sessionId")
    private LunchSession lunchSession;

    private String username;

}
