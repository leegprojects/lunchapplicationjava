package com.swe.challenge.martin.LunchApplication.utils;

import java.util.function.Function;

public class StringUtils {

    public static String removeMultipleSpace(String s) {
        //removes trailing and leading spaces and then replaces all multiiple space with only one space
        return s.trim().replaceAll("\\s+", " ");
    }

    public static String replaceSpaceInCookieString(String s) {
        //replaces space with a "+" sign as space is invalid in cookie value
        return s.replace(" ", "+");
    }

    public static String replacePlusSignWithSpaceFromCookieString(String s) {
        //replaces a "+" sign with space as "+" is used in cookie value to represent spaces
        return s.replace("+", " ");
    }

    public static String transformUsernameForCookieValue(String s) {
        //chains multiple processing functions into one
        Function<String, String> removeMultipleSpaceStep = StringUtils::removeMultipleSpace;
        Function<String, String> replaceSpaceInCookieStringStep = StringUtils::replaceSpaceInCookieString;

        Function<String, String> transformUsernameForCookieValueProcess = removeMultipleSpaceStep.andThen(replaceSpaceInCookieStringStep);

        return transformUsernameForCookieValueProcess.apply(s);
    }
}
