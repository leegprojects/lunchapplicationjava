package com.swe.challenge.martin.LunchApplication.validation.annotations;

import com.swe.challenge.martin.LunchApplication.validation.validator.LiveOrTerminatedSessionValidator;
import jakarta.validation.Constraint;
import jakarta.validation.Payload;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Constraint(validatedBy = LiveOrTerminatedSessionValidator.class)
@Target( {ElementType.PARAMETER, ElementType.METHOD, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface LiveOrTerminatedSessionConstraint {
    String message() default "Invalid session";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
