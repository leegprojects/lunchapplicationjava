package com.swe.challenge.martin.LunchApplication.repository;

import com.swe.challenge.martin.LunchApplication.repository.model.LunchSession;
import com.swe.challenge.martin.LunchApplication.repository.model.LunchSessionUser;
import com.swe.challenge.martin.LunchApplication.repository.model.RestaurantChoice;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface LunchSessionUserDAO extends JpaRepository<LunchSessionUser, Long> {

    @Query("""
              SELECT u FROM LunchSessionUser u WHERE u.lunchSession.sessionId = :sessionId and u.username = :username
            """)
    Optional<LunchSessionUser> findLunchSessionUserBySessionIdAndUsername(@Param("sessionId") Long sessionId, @Param("username") String username);

}
