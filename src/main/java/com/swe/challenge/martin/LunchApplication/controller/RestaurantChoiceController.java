package com.swe.challenge.martin.LunchApplication.controller;

import com.swe.challenge.martin.LunchApplication.constants.UrlConstants;
import com.swe.challenge.martin.LunchApplication.dto.request.CreateRestaurantNameRequest;
import com.swe.challenge.martin.LunchApplication.dto.request.UpdateRestaurantNameRequest;
import com.swe.challenge.martin.LunchApplication.dto.response.RestaurantChoiceResponseDTO;
import com.swe.challenge.martin.LunchApplication.repository.model.RestaurantChoice;
import com.swe.challenge.martin.LunchApplication.service.RestaurantChoiceService;
import com.swe.challenge.martin.LunchApplication.validation.annotations.LiveOrTerminatedSessionConstraint;
import com.swe.challenge.martin.LunchApplication.validation.annotations.LiveSessionConstraint;
import com.swe.challenge.martin.LunchApplication.validation.annotations.RestaurantChoiceConstraint;
import com.swe.challenge.martin.LunchApplication.validation.error.ErrorValidationMessage;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.validation.Valid;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(UrlConstants.RESTAURANT_CHOICE)
public class RestaurantChoiceController {

    private final RestaurantChoiceService restaurantChoiceService;

    public RestaurantChoiceController(RestaurantChoiceService restaurantChoiceService) {
        this.restaurantChoiceService = restaurantChoiceService;
    }

    @GetMapping(value = UrlConstants.GET_RESTAURANT_LIST + "/{sessionId}")
    public ResponseEntity<List<RestaurantChoiceResponseDTO>> getRestaurantList(
            @LiveOrTerminatedSessionConstraint @PathVariable Long sessionId) {
        return new ResponseEntity<>(restaurantChoiceService.getRestaurantList(sessionId), HttpStatus.OK);
    }

    @PostMapping(value = "/{sessionId}")
    public ResponseEntity<RestaurantChoiceResponseDTO> createRestaurantChoice(
            HttpServletRequest httpServletRequest,
            @LiveSessionConstraint @PathVariable Long sessionId,
            @Valid @RequestBody CreateRestaurantNameRequest createRestaurantNameRequest) {
        return new ResponseEntity<>(restaurantChoiceService.createRestaurantChoice(httpServletRequest,sessionId, createRestaurantNameRequest), HttpStatus.OK);
    }

    @GetMapping(value = "/{sessionId}")
    public ResponseEntity<RestaurantChoiceResponseDTO> getRestaurantChoice(
            @LiveSessionConstraint @PathVariable Long sessionId,
            @NotBlank(message = ErrorValidationMessage.USERNAME_CANNOT_BE_BLANK) @RequestParam String username
            ) {
        return new ResponseEntity<>(restaurantChoiceService.getRestaurantChoice(sessionId, username), HttpStatus.OK);
    }

    @PatchMapping(value = "/{sessionId}/{restaurantChoiceId}")
    public ResponseEntity<RestaurantChoiceResponseDTO> updateRestaurantChoice(
            HttpServletRequest httpServletRequest,
            @LiveSessionConstraint @PathVariable Long sessionId,
            @RestaurantChoiceConstraint @PathVariable Long restaurantChoiceId,
            @Valid @RequestBody UpdateRestaurantNameRequest updateRestaurantNameRequest) {
        return new ResponseEntity<>(restaurantChoiceService.updateRestaurantChoice(httpServletRequest, sessionId, restaurantChoiceId, updateRestaurantNameRequest), HttpStatus.OK);
    }

    @DeleteMapping(value = "/{sessionId}/{restaurantChoiceId}")
    public ResponseEntity<String> deleteRestaurantChoice(
            HttpServletRequest httpServletRequest,
            @LiveSessionConstraint @PathVariable Long sessionId,
            @RestaurantChoiceConstraint @PathVariable Long restaurantChoiceId) {

        restaurantChoiceService.deleteRestaurantChoice(httpServletRequest,sessionId, restaurantChoiceId);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
