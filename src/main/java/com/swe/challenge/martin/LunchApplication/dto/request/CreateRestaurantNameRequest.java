package com.swe.challenge.martin.LunchApplication.dto.request;

import com.swe.challenge.martin.LunchApplication.validation.error.ErrorValidationMessage;
import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
public class CreateRestaurantNameRequest extends UsernameRequest {

    @NotBlank(message = ErrorValidationMessage.RESTAURANT_CHOICE_NAME_CANNOT_BE_BLANK)
    private String restaurantName;
}
