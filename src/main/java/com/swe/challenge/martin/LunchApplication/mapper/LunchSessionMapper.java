package com.swe.challenge.martin.LunchApplication.mapper;

import com.swe.challenge.martin.LunchApplication.constants.SessionStatus;
import com.swe.challenge.martin.LunchApplication.dto.response.LunchSessionResponseDTO;
import com.swe.challenge.martin.LunchApplication.dto.response.RestaurantChoiceResponseDTO;
import com.swe.challenge.martin.LunchApplication.repository.model.LunchSession;
import com.swe.challenge.martin.LunchApplication.repository.model.RestaurantChoice;

public class LunchSessionMapper {
    public static LunchSessionResponseDTO lunchSessionToDto(LunchSession lunchSession) {
        return LunchSessionResponseDTO.builder()
                .createdBy(lunchSession.getCreatedBy())
                .sessionId(lunchSession.getSessionId())
                .sessionUuid(lunchSession.getSessionUuid())
                .createdAt(lunchSession.getCreatedAt())
                .sessionStatus(lunchSession.getSessionStatus().ordinal())
                .build();
    }
}
