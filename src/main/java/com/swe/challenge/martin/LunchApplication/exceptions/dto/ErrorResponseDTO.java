package com.swe.challenge.martin.LunchApplication.exceptions.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;

@Getter
@Setter
@Builder
public class ErrorResponseDTO {

    private Integer status;

    private String error;

    private String errorDetails;

}
