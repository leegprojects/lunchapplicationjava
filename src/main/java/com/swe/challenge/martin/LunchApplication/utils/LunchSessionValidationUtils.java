package com.swe.challenge.martin.LunchApplication.utils;

import com.swe.challenge.martin.LunchApplication.constants.SessionStatus;
import com.swe.challenge.martin.LunchApplication.exceptions.constants.LunchApplicationException;
import com.swe.challenge.martin.LunchApplication.exceptions.constants.LunchApplicationExceptionMessage;
import com.swe.challenge.martin.LunchApplication.repository.model.LunchSession;

public class LunchSessionValidationUtils {

    public static boolean isLunchSessionExpired(LunchSession lunchSession) {
        return lunchSession.getSessionStatus().equals(SessionStatus.EXPIRED);
    }

    public static void terminateSession(LunchSession lunchSession) {
        checkIfSessionValid(lunchSession, LunchApplicationExceptionMessage.FAILED_TO_TERMINATE_SESSION_EXPIRED_SESSION, LunchApplicationExceptionMessage.FAILED_TO_TERMINATE_SESSION_TERMINATED_SESSION);
    }

    public static void checkIfSessionValid(LunchSession lunchSession, LunchApplicationExceptionMessage sessionExpiredError, LunchApplicationExceptionMessage sessionTerminatedError) {
        if (lunchSession.getSessionStatus().equals(SessionStatus.EXPIRED))
            throw LunchApplicationException.badRequest(sessionExpiredError);
        else if (lunchSession.getSessionStatus().equals(SessionStatus.TERMINATED))
            throw LunchApplicationException.badRequest(sessionTerminatedError);
    }

    public static void checkIfSessionMatches(LunchSession lunchSession, Long currentSessionId, LunchApplicationExceptionMessage errorMessage) {
        if (!lunchSession.getSessionId().equals(currentSessionId))
            throw  LunchApplicationException.badRequest(errorMessage);
    }


}
