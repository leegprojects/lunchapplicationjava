package com.swe.challenge.martin.LunchApplication.service;

import com.swe.challenge.martin.LunchApplication.dto.request.CreateRestaurantNameRequest;
import com.swe.challenge.martin.LunchApplication.dto.request.UpdateRestaurantNameRequest;
import com.swe.challenge.martin.LunchApplication.dto.response.RestaurantChoiceResponseDTO;
import com.swe.challenge.martin.LunchApplication.exceptions.constants.LunchApplicationException;
import com.swe.challenge.martin.LunchApplication.exceptions.constants.LunchApplicationExceptionMessage;
import com.swe.challenge.martin.LunchApplication.mapper.RestaurantChoiceMapper;
import com.swe.challenge.martin.LunchApplication.repository.LunchSessionDAO;
import com.swe.challenge.martin.LunchApplication.repository.RestaurantChoiceDAO;
import com.swe.challenge.martin.LunchApplication.repository.model.LunchSession;
import com.swe.challenge.martin.LunchApplication.repository.model.RestaurantChoice;
import com.swe.challenge.martin.LunchApplication.utils.CookieUtils;
import com.swe.challenge.martin.LunchApplication.utils.RestaurantChoiceValidationUtils;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.transaction.Transactional;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class RestaurantChoiceService {

    private final RestaurantChoiceDAO restaurantChoiceDAO;

    private final LunchSessionDAO lunchSessionDAO;

    public RestaurantChoiceService(RestaurantChoiceDAO restaurantChoiceDAO, LunchSessionDAO lunchSessionDAO) {
        this.restaurantChoiceDAO = restaurantChoiceDAO;
        this.lunchSessionDAO = lunchSessionDAO;
    }

    public Optional<RestaurantChoice> getRestaurantChoiceDetailsForValidation(Long restaurantChoiceId) {
        return restaurantChoiceDAO.findById(restaurantChoiceId);
    }

    public List<RestaurantChoiceResponseDTO> getRestaurantList(Long sessionId) {
        List<RestaurantChoice> restaurantChoices = restaurantChoiceDAO.findRestaurantChoicesBySessionId(sessionId).orElse(new ArrayList<>());

        return restaurantChoices.stream().map(RestaurantChoiceMapper::RestaurantChoiceToDto).toList();
    }

    public RestaurantChoiceResponseDTO getRestaurantChoice(Long sessionId, String username) {
        return RestaurantChoiceMapper.RestaurantChoiceToDto(restaurantChoiceDAO.findRestaurantChoiceBySessionIdAndUsername(sessionId, username).orElseThrow(() ->
                LunchApplicationException.badRequest(LunchApplicationExceptionMessage.RESTAURANT_CHOICE_NOT_FOUND)
        ));
    }

    @Transactional
    public RestaurantChoiceResponseDTO updateRestaurantChoice(HttpServletRequest httpServletRequest, Long sessionId, Long restaurantChoiceId, UpdateRestaurantNameRequest request) {
        RestaurantChoice restaurantChoice = restaurantChoiceDAO.findById(restaurantChoiceId).orElseThrow();
        RestaurantChoiceValidationUtils.updateRestaurantChoice(restaurantChoice, sessionId, httpServletRequest.getCookies());

        restaurantChoice.setRestaurantName(request.getRestaurantName());

        return RestaurantChoiceMapper.RestaurantChoiceToDto(restaurantChoiceDAO.save(restaurantChoice));
    }

    @Transactional
    public void deleteRestaurantChoice(HttpServletRequest httpServletRequest,Long sessionId, Long restaurantChoiceId) {
        RestaurantChoice restaurantChoice = restaurantChoiceDAO.findById(restaurantChoiceId).orElseThrow();
        RestaurantChoiceValidationUtils.deleteRestaurantChoice(restaurantChoice, sessionId, httpServletRequest.getCookies());

        restaurantChoiceDAO.delete(restaurantChoice);
    }

    public RestaurantChoiceResponseDTO createRestaurantChoice(HttpServletRequest httpServletRequest, Long sessionId, CreateRestaurantNameRequest request) {
        LunchSession lunchSession = lunchSessionDAO.findById(sessionId).orElseThrow(() ->
                LunchApplicationException.badRequest(LunchApplicationExceptionMessage.SESSION_NOT_FOUND)
        );

        //prevents user from creating multiple restaurant choice in same session
        restaurantChoiceDAO.findRestaurantChoiceBySessionIdAndUsername(sessionId, request.getUsername())
                .ifPresent(restaurantChoice -> {
                    throw LunchApplicationException.badRequest(LunchApplicationExceptionMessage.FAILED_TO_CREATE_RESTAURANT_CHOICE_ALREADY_CREATED);
        });

        RestaurantChoice restaurantChoice = RestaurantChoice.builder()
                .createdBy(request.getUsername())
                .restaurantName(request.getRestaurantName())
                .lunchSession(lunchSession)
                .build();

        RestaurantChoiceValidationUtils.createRestaurantChoice(restaurantChoice);

        restaurantChoice = restaurantChoiceDAO.save(restaurantChoice);

        return RestaurantChoiceMapper.RestaurantChoiceToDto(restaurantChoice);
    }
}
