package com.swe.challenge.martin.LunchApplication.repository.model;

import jakarta.persistence.*;
import lombok.*;

@Entity
@Getter
@Setter
@Table(name = "restaurant_choice")
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RestaurantChoice {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String createdBy;

    private String restaurantName;

    @ManyToOne
    @JoinColumn(name = "sessionId")
    private LunchSession lunchSession;
}
