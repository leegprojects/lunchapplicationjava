package com.swe.challenge.martin.LunchApplication.validation.annotations;

import com.swe.challenge.martin.LunchApplication.validation.validator.RestaurantChoiceValidator;
import jakarta.validation.Constraint;
import jakarta.validation.Payload;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Constraint(validatedBy = RestaurantChoiceValidator.class)
@Target( {ElementType.PARAMETER, ElementType.METHOD, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface RestaurantChoiceConstraint {
    String message() default "Invalid restaurant choice";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
