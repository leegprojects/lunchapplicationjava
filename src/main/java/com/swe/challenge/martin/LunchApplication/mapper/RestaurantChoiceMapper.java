package com.swe.challenge.martin.LunchApplication.mapper;

import com.swe.challenge.martin.LunchApplication.dto.response.RestaurantChoiceResponseDTO;
import com.swe.challenge.martin.LunchApplication.repository.model.RestaurantChoice;
import org.springframework.stereotype.Service;

public class RestaurantChoiceMapper {

    public static RestaurantChoiceResponseDTO RestaurantChoiceToDto(RestaurantChoice restaurantChoice) {
        return RestaurantChoiceResponseDTO.builder()
                .createdBy(restaurantChoice.getCreatedBy())
                .id(restaurantChoice.getId())
                .restaurantName(restaurantChoice.getRestaurantName())
                .sessionId(restaurantChoice.getLunchSession().getSessionId())
                .build();
    }
}
