package com.swe.challenge.martin.LunchApplication.utils;

import java.util.Random;

public class RandomUtils {

    public static Integer getRandom(Integer size) {
        Random random = new Random();
        return random.nextInt(size);
    }
}
