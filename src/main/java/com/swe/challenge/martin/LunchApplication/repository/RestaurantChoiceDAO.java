package com.swe.challenge.martin.LunchApplication.repository;

import com.swe.challenge.martin.LunchApplication.repository.model.RestaurantChoice;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface RestaurantChoiceDAO extends JpaRepository<RestaurantChoice, Long> {

    @Query("""
              SELECT r FROM RestaurantChoice r WHERE r.lunchSession.sessionId = :sessionId
            """)
    Optional<List<RestaurantChoice>> findRestaurantChoicesBySessionId(@Param("sessionId") Long sessionId);

    @Query("""
              SELECT r FROM RestaurantChoice r WHERE r.lunchSession.sessionId = :sessionId and r.createdBy = :createdBy
            """)
    Optional<RestaurantChoice> findRestaurantChoiceBySessionIdAndUsername(@Param("sessionId") Long sessionId, @Param("createdBy") String createdBy);

    @Query("""
              SELECT r FROM RestaurantChoice r WHERE r.id = :id and r.lunchSession.sessionId = :sessionId
            """)
    Optional<RestaurantChoice> findRestaurantChoiceBySessionIdAndRestaurantChoiceId(@Param("id") Long id, @Param("sessionId") Long sessionId);

}
