package com.swe.challenge.martin.LunchApplication.exceptions.constants;

import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ProblemDetail;
import org.springframework.web.ErrorResponseException;

public class LunchApplicationException extends ErrorResponseException {

    private LunchApplicationException(HttpStatus httpStatus, LunchApplicationExceptionMessage lunchApplicationExceptionMessage) {
        super(httpStatus, lunchApplicationExceptionMessage.getProblemDetail(), null);
    }

    private LunchApplicationException(HttpStatus httpStatus, LunchApplicationExceptionMessage lunchApplicationExceptionMessage, String error) {
        super(httpStatus, lunchApplicationExceptionMessage.getProblemDetail(error), null);
    }

    public static LunchApplicationException badRequest(LunchApplicationExceptionMessage lunchApplicationExceptionMessage) {
        return new LunchApplicationException(HttpStatus.BAD_REQUEST, lunchApplicationExceptionMessage);
    }

    public static LunchApplicationException badRequest(LunchApplicationExceptionMessage lunchApplicationExceptionMessage, String error) {
        return new LunchApplicationException(HttpStatus.BAD_REQUEST, lunchApplicationExceptionMessage, error);
    }
}
