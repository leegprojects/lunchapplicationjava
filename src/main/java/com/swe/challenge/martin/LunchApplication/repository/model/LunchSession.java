package com.swe.challenge.martin.LunchApplication.repository.model;

import com.swe.challenge.martin.LunchApplication.constants.SessionStatus;
import jakarta.persistence.*;
import lombok.*;

import java.time.LocalDateTime;
import java.util.List;

@Entity
@Getter
@Setter
@Builder
@Table(name = "lunch_session")
@NoArgsConstructor
@AllArgsConstructor
public class LunchSession {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long sessionId;

    private String sessionUuid;

    @Enumerated
    private SessionStatus sessionStatus;

    @OneToMany(mappedBy = "lunchSession")
    private List<RestaurantChoice> restaurantChoice;

    private LocalDateTime createdAt;

    @OneToMany(mappedBy = "lunchSession")
    private List<LunchSessionUser> lunchSessionUsers;

    private String createdBy;

}
