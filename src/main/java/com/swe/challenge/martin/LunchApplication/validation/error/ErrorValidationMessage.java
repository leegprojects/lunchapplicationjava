package com.swe.challenge.martin.LunchApplication.validation.error;

public class ErrorValidationMessage {

    public static final String USERNAME_CANNOT_BE_BLANK = "Username cannot be blank";
    public static final String RESTAURANT_CHOICE_NAME_CANNOT_BE_BLANK = "Name of restaurant choice cannot be blank";
    public static final String VALIDATION_FAILED = "Validation failed";
}
