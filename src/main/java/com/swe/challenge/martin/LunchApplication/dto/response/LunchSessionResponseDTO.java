package com.swe.challenge.martin.LunchApplication.dto.response;

import com.swe.challenge.martin.LunchApplication.constants.SessionStatus;
import com.swe.challenge.martin.LunchApplication.repository.model.RestaurantChoice;
import jakarta.persistence.*;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.List;

@Setter
@Getter
@Builder
public class LunchSessionResponseDTO {
    private Long sessionId;

    private String sessionUuid;

    private Integer sessionStatus;

    private LocalDateTime createdAt;

    private String createdBy;
}
