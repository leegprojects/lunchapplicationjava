package com.swe.challenge.martin.LunchApplication.utils;

import com.swe.challenge.martin.LunchApplication.exceptions.constants.LunchApplicationException;
import com.swe.challenge.martin.LunchApplication.exceptions.constants.LunchApplicationExceptionMessage;
import com.swe.challenge.martin.LunchApplication.repository.model.LunchSession;
import com.swe.challenge.martin.LunchApplication.repository.model.RestaurantChoice;
import jakarta.servlet.http.Cookie;

public class RestaurantChoiceValidationUtils {

    public static void updateRestaurantChoice(RestaurantChoice restaurantChoice, Long currentSessionId, Cookie[] cookies) {
        LunchSession lunchSession = restaurantChoice.getLunchSession();
        LunchSessionValidationUtils.checkIfSessionValid(lunchSession, LunchApplicationExceptionMessage.FAILED_TO_UPDATE_RESTAURANT_CHOICE_EXPIRED_SESSION, LunchApplicationExceptionMessage.FAILED_TO_UPDATE_RESTAURANT_CHOICE_TERMINATED_SESSION);
        LunchSessionValidationUtils.checkIfSessionMatches(lunchSession, currentSessionId, LunchApplicationExceptionMessage.RESTAURANT_CHOICE_DOES_NOT_BELONG_TO_CURRENT_SESSION);
        validateUserCookie(restaurantChoice, cookies, LunchApplicationExceptionMessage.FAILED_TO_UPDATE_RESTAURANT_CHOICE_NOT_OWNER);


    }

    public static void deleteRestaurantChoice(RestaurantChoice restaurantChoice, Long currentSessionId, Cookie[] cookies) {
        LunchSession lunchSession = restaurantChoice.getLunchSession();
        LunchSessionValidationUtils.checkIfSessionValid(lunchSession, LunchApplicationExceptionMessage.FAILED_TO_DELETE_RESTAURANT_CHOICE_EXPIRED_SESSION, LunchApplicationExceptionMessage.FAILED_TO_DELETE_RESTAURANT_CHOICE_TERMINATED_SESSION);
        LunchSessionValidationUtils.checkIfSessionMatches(lunchSession, currentSessionId, LunchApplicationExceptionMessage.RESTAURANT_CHOICE_DOES_NOT_BELONG_TO_CURRENT_SESSION);

        validateUserCookie(restaurantChoice, cookies, LunchApplicationExceptionMessage.FAILED_TO_DELETE_RESTAURANT_CHOICE_NOT_OWNER);
    }

    public static void createRestaurantChoice(RestaurantChoice restaurantChoice) {
        LunchSession lunchSession = restaurantChoice.getLunchSession();
        LunchSessionValidationUtils.checkIfSessionValid(lunchSession, LunchApplicationExceptionMessage.FAILED_TO_CREATE_RESTAURANT_CHOICE_EXPIRED_SESSION, LunchApplicationExceptionMessage.FAILED_TO_CREATE_RESTAURANT_CHOICE_TERMINATED_SESSION);
    }

    public static void validateUserCookie(RestaurantChoice restaurantChoice, Cookie[] cookies, LunchApplicationExceptionMessage errorMessage) {
        //get "user" cookie from request. The "user" cookie is created previously when user joins the session
        Cookie userCookie = CookieUtils.getSessionCookie(cookies, restaurantChoice.getLunchSession().getSessionUuid());
        //restaurant choice cannot be updated if "user" cookie does not exist or username in userCookie does not match the createdBy of the restaurant choice,
        if (userCookie == null || !CookieUtils.getUsername(userCookie).equals(restaurantChoice.getCreatedBy())) {
            throw LunchApplicationException.badRequest(errorMessage);
        }
    }
}
