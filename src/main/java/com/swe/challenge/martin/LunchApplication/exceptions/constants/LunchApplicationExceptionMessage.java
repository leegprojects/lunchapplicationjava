package com.swe.challenge.martin.LunchApplication.exceptions.constants;

import lombok.Getter;
import org.springframework.http.ProblemDetail;

@Getter
public enum LunchApplicationExceptionMessage {

    FAILED_TO_UPDATE_RESTAURANT_CHOICE_EXPIRED_SESSION(1000, "Failed to update restaurant choice because session has expired"),
    FAILED_TO_UPDATE_RESTAURANT_CHOICE_TERMINATED_SESSION(1001, "Failed to update restaurant choice because session has been terminated"),
    FAILED_TO_DELETE_RESTAURANT_CHOICE_EXPIRED_SESSION(1002, "Failed to delete restaurant choice because session has expired"),
    FAILED_TO_DELETE_RESTAURANT_CHOICE_TERMINATED_SESSION(1003, "Failed to delete restaurant choice because session has been terminated"),
    FAILED_TO_CREATE_RESTAURANT_CHOICE_EXPIRED_SESSION(1004, "Failed to create restaurant choice because session has expired"),
    FAILED_TO_CREATE_RESTAURANT_CHOICE_TERMINATED_SESSION(1005, "Failed to create restaurant choice because session has been terminated"),
    INVALID_RESTAURANT_CHOICE(1006, "RestaurantChoice id cannot be null or empty and must be a positive number"),
    RESTAURANT_CHOICE_DOES_NOT_BELONG_TO_CURRENT_SESSION(1007, "Restaurant choice does not belong to current session"),
    RESTAURANT_CHOICE_NOT_FOUND(1008, "Restaurant choice not found in records"),
    INVALID_RESTAURANT_CHOICE_NAME(1009, "Restaurant choice name cannot be null or empty"),
    FAILED_TO_UPDATE_RESTAURANT_CHOICE_NOT_OWNER(1010, "Failed to update restaurant choice because restaurant choice was not created by current user"),
    FAILED_TO_DELETE_RESTAURANT_CHOICE_NOT_OWNER(1011, "Failed to delete restaurant choice because restaurant choice was not created by current user"),
    FAILED_TO_CREATE_RESTAURANT_CHOICE_ALREADY_CREATED(1012, "Failed to create restaurant choice because user has already created for the current session"),

    FAILED_TO_TERMINATE_SESSION_EXPIRED_SESSION(2000, "Failed to terminate a session that has already expired"),
    FAILED_TO_TERMINATE_SESSION_TERMINATED_SESSION(2001, "Failed to terminate a session that has already been terminated"),
    SESSION_NOT_FOUND(2002, "Session not found in records"),
    INVALID_SESSION_ID(2003, "Session id cannot be null or empty and must be a positive number"),
    TERMINATED_OR_EXPIRED_SESSION(2004, "Session has already expired or have been terminated"),
    FAILED_TO_TERMINATE_SESSION_NOT_OWNER_OF_SESSION(2005,"Failed to terminate this session because you are not the owner of this session"),

    INVALID_USER_NAME(3000, "Username cannot be null or empty"),
    USER_NAME_ALREADY_EXIST_IN_CURRENT_SESSION(3001, "Cannot have same username in the same session"),


    OTHER_ERROR(9999, "other error");

    private final Integer errorCode;

    private final String errorMessage;

    LunchApplicationExceptionMessage(Integer errorCode, String errorMessage) {
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
    }

    public ProblemDetail getProblemDetail() {
        ProblemDetail problemDetail = ProblemDetail.forStatus(this.errorCode);
        problemDetail.setDetail(this.errorMessage);
        return problemDetail;
    }

    public ProblemDetail getProblemDetail(String error) {
        ProblemDetail problemDetail = ProblemDetail.forStatus(this.errorCode);
        problemDetail.setTitle(error);
        problemDetail.setDetail(this.errorMessage);
        return problemDetail;
    }
}
