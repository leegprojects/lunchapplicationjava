package com.swe.challenge.martin.LunchApplication.repository;

import com.swe.challenge.martin.LunchApplication.repository.model.LunchSession;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LunchSessionDAO extends JpaRepository<LunchSession, Long> {
}
