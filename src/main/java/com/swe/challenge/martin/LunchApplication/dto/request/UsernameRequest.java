package com.swe.challenge.martin.LunchApplication.dto.request;

import com.swe.challenge.martin.LunchApplication.validation.error.ErrorValidationMessage;
import jakarta.validation.constraints.NotBlank;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UsernameRequest {

    @NotBlank(message = ErrorValidationMessage.USERNAME_CANNOT_BE_BLANK)
    private String username;
}
