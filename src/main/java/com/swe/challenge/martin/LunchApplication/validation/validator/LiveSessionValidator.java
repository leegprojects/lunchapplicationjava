package com.swe.challenge.martin.LunchApplication.validation.validator;

import com.swe.challenge.martin.LunchApplication.constants.SessionStatus;
import com.swe.challenge.martin.LunchApplication.exceptions.constants.LunchApplicationException;
import com.swe.challenge.martin.LunchApplication.exceptions.constants.LunchApplicationExceptionMessage;
import com.swe.challenge.martin.LunchApplication.repository.model.LunchSession;
import com.swe.challenge.martin.LunchApplication.service.LunchSessionService;
import com.swe.challenge.martin.LunchApplication.validation.annotations.LiveSessionConstraint;
import com.swe.challenge.martin.LunchApplication.validation.error.ErrorValidationMessage;
import jakarta.validation.*;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class LiveSessionValidator implements ConstraintValidator<LiveSessionConstraint, Long> {

    private final LunchSessionService lunchSessionService;

    public LiveSessionValidator(LunchSessionService lunchSessionService) {
        this.lunchSessionService = lunchSessionService;
    }

    @Override
    public void initialize(LiveSessionConstraint constraintAnnotation) {
        ConstraintValidator.super.initialize(constraintAnnotation);
    }

    @Override
    public boolean isValid(Long sessionId, ConstraintValidatorContext context) {
        if (sessionId == null || sessionId <= 0) {
            throw LunchApplicationException.badRequest(LunchApplicationExceptionMessage.INVALID_SESSION_ID, ErrorValidationMessage.VALIDATION_FAILED);
        }

        Optional<LunchSession> lunchSessionOptional = lunchSessionService.getSessionDetailsForValidation(sessionId);
        if (lunchSessionOptional.isEmpty()) {
            throw LunchApplicationException.badRequest(LunchApplicationExceptionMessage.SESSION_NOT_FOUND, ErrorValidationMessage.VALIDATION_FAILED);
        } else {
            LunchSession lunchSession = lunchSessionOptional.get();
            if (lunchSession.getSessionStatus().equals(SessionStatus.TERMINATED) || lunchSession.getSessionStatus().equals(SessionStatus.EXPIRED)) {
                throw LunchApplicationException.badRequest(LunchApplicationExceptionMessage.TERMINATED_OR_EXPIRED_SESSION, ErrorValidationMessage.VALIDATION_FAILED);
            }
        }

        return true;
    }


}
