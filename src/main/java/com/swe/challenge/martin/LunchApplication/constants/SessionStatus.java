package com.swe.challenge.martin.LunchApplication.constants;

import lombok.Getter;

@Getter
public enum SessionStatus {
    STARTED,
    TERMINATED,
    EXPIRED
}
