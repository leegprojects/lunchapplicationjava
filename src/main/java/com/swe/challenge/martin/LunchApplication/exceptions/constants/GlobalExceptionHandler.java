package com.swe.challenge.martin.LunchApplication.exceptions.constants;

import com.swe.challenge.martin.LunchApplication.exceptions.dto.ErrorResponseDTO;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.validation.ConstraintViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(LunchApplicationException.class)
    public ResponseEntity<ErrorResponseDTO> handleLunchApplicationException(LunchApplicationException lunchApplicationException, HttpServletRequest request) {
        ErrorResponseDTO errorResponseDTO = ErrorResponseDTO.builder()
                .error(lunchApplicationException.getBody().getTitle())
                .errorDetails(lunchApplicationException.getBody().getDetail())
                .status(HttpStatus.BAD_REQUEST.value())
                .build();

        System.out.println("request failed for URI : " + request.getRequestURI());
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponseDTO);

    }
}
