package com.swe.challenge.martin.LunchApplication.service;

import com.swe.challenge.martin.LunchApplication.constants.SessionStatus;
import com.swe.challenge.martin.LunchApplication.dto.request.UsernameRequest;
import com.swe.challenge.martin.LunchApplication.dto.response.LunchSessionResponseDTO;
import com.swe.challenge.martin.LunchApplication.dto.response.SessionUuidResponseDTO;
import com.swe.challenge.martin.LunchApplication.exceptions.constants.LunchApplicationException;
import com.swe.challenge.martin.LunchApplication.exceptions.constants.LunchApplicationExceptionMessage;
import com.swe.challenge.martin.LunchApplication.mapper.LunchSessionMapper;
import com.swe.challenge.martin.LunchApplication.repository.LunchSessionDAO;
import com.swe.challenge.martin.LunchApplication.repository.LunchSessionUserDAO;
import com.swe.challenge.martin.LunchApplication.repository.RestaurantChoiceDAO;
import com.swe.challenge.martin.LunchApplication.repository.model.LunchSession;
import com.swe.challenge.martin.LunchApplication.repository.model.LunchSessionUser;
import com.swe.challenge.martin.LunchApplication.repository.model.RestaurantChoice;
import com.swe.challenge.martin.LunchApplication.utils.CookieUtils;
import com.swe.challenge.martin.LunchApplication.utils.RandomUtils;
import com.swe.challenge.martin.LunchApplication.utils.UuidGenerator;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.transaction.Transactional;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class LunchSessionService {

    private final LunchSessionDAO lunchSessionDAO;

    private final RestaurantChoiceDAO restaurantChoiceDAO;

    private final LunchSessionUserDAO lunchSessionUserDAO;

    public LunchSessionService(LunchSessionDAO lunchSessionDAO, RestaurantChoiceDAO restaurantChoiceDAO, LunchSessionUserDAO lunchSessionUserDAO) {
        this.lunchSessionDAO = lunchSessionDAO;
        this.restaurantChoiceDAO = restaurantChoiceDAO;
        this.lunchSessionUserDAO = lunchSessionUserDAO;
    }

    @Transactional
    public SessionUuidResponseDTO createSession(HttpServletResponse httpServletResponse, UsernameRequest usernameRequest) {
        LocalDateTime now = LocalDateTime.now();
        String sessionUuid = UuidGenerator.generateUUID();
        LunchSession lunchSession = LunchSession.builder()
                .sessionStatus(SessionStatus.STARTED)
                .createdAt(now)
                .sessionUuid(sessionUuid)
                .createdBy(usernameRequest.getUsername())
                .build();

        lunchSession = lunchSessionDAO.saveAndFlush(lunchSession);

        //saves lunchsessionuser
        LunchSessionUser lunchSessionUser = LunchSessionUser.builder()
                .lunchSession(lunchSession)
                .username(usernameRequest.getUsername())
                .build();

        lunchSessionUserDAO.saveAndFlush(lunchSessionUser);

        //set cookie to identify owner of the lunch session
        httpServletResponse.addCookie(CookieUtils.createOwnerCookie(sessionUuid, usernameRequest.getUsername()));

        SessionUuidResponseDTO sessionUuidResponseDTO = new SessionUuidResponseDTO();
        sessionUuidResponseDTO.setSessionId(lunchSession.getSessionId());
        sessionUuidResponseDTO.setSessionUuid(lunchSession.getSessionUuid());
        return sessionUuidResponseDTO;
    }

    public LunchSession getSessionDetails(Long sessionId) {
        return lunchSessionDAO.findById(sessionId).orElseThrow(() ->
                LunchApplicationException.badRequest(LunchApplicationExceptionMessage.SESSION_NOT_FOUND)
        );
    }

    public Optional<LunchSession> getSessionDetailsForValidation(Long sessionId) {
        return lunchSessionDAO.findById(sessionId);
    }

    @Transactional
    public LunchSessionResponseDTO terminateSession(HttpServletRequest httpServletRequest, Long sessionId) {
        LunchSession lunchSession = getSessionDetails(sessionId);

        //get session cookie from request. The session cookie is created only if user has previously created or joined a session
        Cookie sessionCookie = CookieUtils.getSessionCookie(httpServletRequest.getCookies(), lunchSession.getSessionUuid());

        //checks if session cookie is an "owner" cookie for the session to be deleted
        if (sessionCookie == null || !CookieUtils.isOwner(sessionCookie)) {
            throw LunchApplicationException.badRequest(LunchApplicationExceptionMessage.FAILED_TO_TERMINATE_SESSION_NOT_OWNER_OF_SESSION);
        }

        List<RestaurantChoice> restaurantChoiceList = restaurantChoiceDAO.findRestaurantChoicesBySessionId(sessionId).orElse(new ArrayList<>());
        //select 1 random restaurant choice from list and delete the remaining restaurant choices in the session
        if (!restaurantChoiceList.isEmpty()) {
            RestaurantChoice selectedRestaurantChoice = restaurantChoiceList.get(RandomUtils.getRandom(restaurantChoiceList.size()));
            List<RestaurantChoice> restaurantChoicesToDelete = restaurantChoiceList.stream().filter(restaurantChoice -> !restaurantChoice.getId().equals(selectedRestaurantChoice.getId())).toList();
            restaurantChoiceDAO.deleteAll(restaurantChoicesToDelete);
        }

        lunchSession.setSessionStatus(SessionStatus.TERMINATED);
        return LunchSessionMapper.lunchSessionToDto(lunchSessionDAO.save(lunchSession));
    }

    @Transactional
    public LunchSessionResponseDTO joinSession(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Long sessionId, String username) {
        LunchSession lunchSession = getSessionDetails(sessionId);
        //check if session cookie already exist in request
        Cookie sessionCookie = CookieUtils.getSessionCookie(httpServletRequest.getCookies(), lunchSession.getSessionUuid());

        //set cookie to identify username in the lunch session if session cookie does not exist
        if (sessionCookie == null) {
            //before setting cookie, check if username already exist in current session. Prevents same session from having users with same name
            lunchSessionUserDAO.findLunchSessionUserBySessionIdAndUsername(sessionId, username).ifPresent(lunchSessionUser -> {
                throw LunchApplicationException.badRequest(LunchApplicationExceptionMessage.USER_NAME_ALREADY_EXIST_IN_CURRENT_SESSION);
            });

            //saves lunchsessionuser
            LunchSessionUser lunchSessionUser = LunchSessionUser.builder()
                    .lunchSession(lunchSession)
                    .username(username)
                    .build();

            lunchSessionUserDAO.saveAndFlush(lunchSessionUser);

            httpServletResponse.addCookie(CookieUtils.createUserCookie(lunchSession.getSessionUuid(), username));
        }

        return LunchSessionMapper.lunchSessionToDto(lunchSession);
    }
}
