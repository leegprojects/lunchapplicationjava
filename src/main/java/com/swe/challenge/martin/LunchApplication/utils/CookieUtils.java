package com.swe.challenge.martin.LunchApplication.utils;

import jakarta.servlet.http.Cookie;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

public class CookieUtils {

    public final static String COOKIE_NAME_OWNER = "owner";
    public final static String COOKIE_NAME_USER = "user";

    public static Cookie createOwnerCookie(String lunchSessionUuid, String username) {
        //OwnerCookie : [sessionUuid]=[owner]_[username]
        //for eg. c3ee64c5a13b42929b94f8d48d63be46=owner_james+tan+jia+hao
        String cookieValue = new StringBuilder()
                .append(COOKIE_NAME_OWNER)
                .append("_")
                .append(StringUtils.transformUsernameForCookieValue(username)).toString();
        Cookie ownerCookie = new Cookie(lunchSessionUuid, cookieValue);
        return setCookieParameters(ownerCookie);
    }

    public static Cookie createUserCookie(String lunchSessionUuid, String username) {
        //UserCookie : [sessionUuid]=[user]_[username]+[username(if there are spaces sent in username)]
        //for eg. c3ee64c5a13b42929b94f8d48d63be46=user_james+tan+jia+hao
        String cookieValue = new StringBuilder()
                .append(COOKIE_NAME_USER)
                .append("_")
                .append(StringUtils.transformUsernameForCookieValue(username)).toString();
        Cookie userCookie = new Cookie(lunchSessionUuid, cookieValue);
        return setCookieParameters(userCookie);
    }

    public static Cookie getSessionCookie(Cookie[] cookies, String sessionUuid) {
        //OwnerCookie : [sessionUuid]=[owner]_[username]+[username(if there are spaces sent in username)]
        //for eg. c3ee64c5a13b42929b94f8d48d63be46=owner_james+tan+jia+hao
        //UserCookie : [sessionUuid]=[user]_[username]+[username(if there are spaces sent in username)]
        //for eg. c3ee64c5a13b42929b94f8d48d63be46=user_james+tan+jia+hao
        if (cookies != null) {
            return Arrays.stream(cookies)
                    .filter(cookie -> cookie.getName().equals(sessionUuid))
                    .findFirst()
                    .orElse(null);
        }
        return null;
    }

    public static Cookie setCookieParameters(Cookie cookie) {
        //currently, lifespan of session cookies are set to 2hrs
        cookie.setMaxAge(7200);
        cookie.setPath("/");
        return cookie;
    }

    public static boolean isOwner(Cookie sessionCookie) {
        //Examples of session cookie values : c3ee64c5a13b42929b94f8d48d63be46=user_james+tan+jia+hao, c3ee64c5a13b42929b94f8d48d63be46=owner_james+tan+jia+hao
        //current function checks if "owner" exist in the sessionCookie
        return sessionCookie.getValue().split("_")[0].equals(COOKIE_NAME_OWNER);
    }

    public static String getUsername(Cookie sessionCookie) {
        //replace '+' sign with ' ' in cookie string eg. 'james+tan+jia+hao' to 'james tan jia hao'
        return StringUtils.replacePlusSignWithSpaceFromCookieString(sessionCookie.getValue().split("_")[1]);
    }

}