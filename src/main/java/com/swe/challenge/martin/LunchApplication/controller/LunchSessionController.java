package com.swe.challenge.martin.LunchApplication.controller;

import com.swe.challenge.martin.LunchApplication.constants.UrlConstants;
import com.swe.challenge.martin.LunchApplication.dto.request.UsernameRequest;
import com.swe.challenge.martin.LunchApplication.dto.response.LunchSessionResponseDTO;
import com.swe.challenge.martin.LunchApplication.dto.response.SessionUuidResponseDTO;
import com.swe.challenge.martin.LunchApplication.mapper.LunchSessionMapper;
import com.swe.challenge.martin.LunchApplication.repository.model.RestaurantChoice;
import com.swe.challenge.martin.LunchApplication.service.LunchSessionService;
import com.swe.challenge.martin.LunchApplication.validation.annotations.LiveOrTerminatedSessionConstraint;
import com.swe.challenge.martin.LunchApplication.validation.annotations.LiveSessionConstraint;
import com.swe.challenge.martin.LunchApplication.validation.error.ErrorValidationMessage;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotBlank;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(UrlConstants.LUNCH_SESSION)
public class LunchSessionController {

    private final LunchSessionService lunchSessionService;


    public LunchSessionController(LunchSessionService lunchSessionService) {
        this.lunchSessionService = lunchSessionService;
    }

    @PostMapping
    public ResponseEntity<SessionUuidResponseDTO> createSession(
            HttpServletResponse httpServletResponse,
            @Valid @RequestBody UsernameRequest usernameRequest) {
        return new ResponseEntity<>(lunchSessionService.createSession(httpServletResponse, usernameRequest), HttpStatus.OK);
    }

    @GetMapping(value = "/{sessionId}" )
    public ResponseEntity<LunchSessionResponseDTO> getSessionDetails(
            @PathVariable("sessionId") @LiveOrTerminatedSessionConstraint Long sessionId) {
        return new ResponseEntity<>(LunchSessionMapper.lunchSessionToDto(lunchSessionService.getSessionDetails(sessionId)), HttpStatus.OK);
    }

    @DeleteMapping(value = "/{sessionId}" )
    public ResponseEntity<LunchSessionResponseDTO> terminateSession(
            HttpServletRequest httpServletRequest,
            @PathVariable("sessionId") @LiveSessionConstraint Long sessionId) {
        return new ResponseEntity<>(lunchSessionService.terminateSession(httpServletRequest, sessionId), HttpStatus.OK);
    }

    @GetMapping(value = UrlConstants.JOIN_SESSION + "/{sessionId}")
    public ResponseEntity<LunchSessionResponseDTO> joinSession(
            HttpServletResponse httpServletResponse,
            HttpServletRequest httpServletRequest,
            @PathVariable("sessionId") @LiveSessionConstraint Long sessionId,
            @NotBlank(message = ErrorValidationMessage.USERNAME_CANNOT_BE_BLANK) @RequestParam String username) {

        return new ResponseEntity<>(lunchSessionService.joinSession(httpServletRequest, httpServletResponse, sessionId, username), HttpStatus.OK);
    }

}
