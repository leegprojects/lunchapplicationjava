package com.swe.challenge.martin.LunchApplication.validation.validator;

import com.swe.challenge.martin.LunchApplication.exceptions.constants.LunchApplicationException;
import com.swe.challenge.martin.LunchApplication.exceptions.constants.LunchApplicationExceptionMessage;
import com.swe.challenge.martin.LunchApplication.repository.model.RestaurantChoice;
import com.swe.challenge.martin.LunchApplication.service.RestaurantChoiceService;
import com.swe.challenge.martin.LunchApplication.validation.annotations.RestaurantChoiceConstraint;
import com.swe.challenge.martin.LunchApplication.validation.error.ErrorValidationMessage;
import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class RestaurantChoiceValidator implements ConstraintValidator<RestaurantChoiceConstraint, Long> {

    private final RestaurantChoiceService restaurantChoiceService;

    public RestaurantChoiceValidator(RestaurantChoiceService restaurantChoiceService) {
        this.restaurantChoiceService = restaurantChoiceService;
    }

    @Override
    public void initialize(RestaurantChoiceConstraint constraintAnnotation) {
        ConstraintValidator.super.initialize(constraintAnnotation);
    }

    @Override
    public boolean isValid(Long restaurantChoiceId, ConstraintValidatorContext context) {
        if (restaurantChoiceId == null || restaurantChoiceId <= 0) {
            throw LunchApplicationException.badRequest(LunchApplicationExceptionMessage.INVALID_RESTAURANT_CHOICE, ErrorValidationMessage.VALIDATION_FAILED);
        }

        Optional<RestaurantChoice> restaurantChoiceOptional = restaurantChoiceService.getRestaurantChoiceDetailsForValidation(restaurantChoiceId);
        if (restaurantChoiceOptional.isEmpty()) {
            throw LunchApplicationException.badRequest(LunchApplicationExceptionMessage.RESTAURANT_CHOICE_NOT_FOUND, ErrorValidationMessage.VALIDATION_FAILED);
        }

        return true;
    }
}
