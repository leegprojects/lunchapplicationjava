package com.swe.challenge.martin.LunchApplication.dto.request;

import com.swe.challenge.martin.LunchApplication.validation.error.ErrorValidationMessage;
import jakarta.validation.constraints.NotBlank;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UpdateRestaurantNameRequest extends UsernameRequest {

    @NotBlank(message = ErrorValidationMessage.RESTAURANT_CHOICE_NAME_CANNOT_BE_BLANK)
    private String restaurantName;
}
