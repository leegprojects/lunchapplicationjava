package com.swe.challenge.martin.LunchApplication.constants;

public class UrlConstants {

    public static final String LUNCH_SESSION = "lunchSession";

    public static final String CREATE_SESSION = "createSession";

    public static final String TERMINATE_SESSION = "terminateSession";

    public static final String JOIN_SESSION = "joinSession";

    public static final String CHECK_SESSION_STATUS = "checkSessionStatus";

    public static final String RESTAURANT_CHOICE = "restaurantChoice";

    public static final String GET_RESTAURANT_LIST = "getList";

}
