package com.swe.challenge.martin.LunchApplication.integrationtest;

import com.swe.challenge.martin.LunchApplication.constants.UrlConstants;
import com.swe.challenge.martin.LunchApplication.dto.request.UsernameRequest;
import com.swe.challenge.martin.LunchApplication.dto.response.SessionUuidResponseDTO;
import com.swe.challenge.martin.LunchApplication.repository.LunchSessionDAO;
import com.swe.challenge.martin.LunchApplication.repository.model.LunchSession;
import com.swe.challenge.martin.LunchApplication.utils.MockUtils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class LunchSessionControllerIT {

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private LunchSessionDAO lunchSessionDAO;


    @Test
    public void createSessionIT() {
        String baseUrl = restTemplate.getRootUri();
        String createSessionUrl = new StringBuilder().append(baseUrl).append("/").append(UrlConstants.LUNCH_SESSION).toString();
        UsernameRequest usernameRequest = MockUtils.createUsernameRequest();

        ResponseEntity<SessionUuidResponseDTO> response = restTemplate.postForEntity(createSessionUrl, usernameRequest, SessionUuidResponseDTO.class);
        Optional<LunchSession> lunchSessionOptional = lunchSessionDAO.findById(response.getBody().getSessionId());
        LunchSession lunchSession = lunchSessionOptional.get();
        assertNotNull(lunchSession);
        //checks if session has been successfully created
        assertEquals(response.getBody().getSessionUuid(), lunchSession.getSessionUuid());
        System.out.println("success");

    }

}
