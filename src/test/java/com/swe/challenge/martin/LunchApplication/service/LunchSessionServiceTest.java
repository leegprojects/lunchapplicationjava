package com.swe.challenge.martin.LunchApplication.service;

import com.swe.challenge.martin.LunchApplication.constants.SessionStatus;
import com.swe.challenge.martin.LunchApplication.dto.request.UsernameRequest;
import com.swe.challenge.martin.LunchApplication.dto.response.LunchSessionResponseDTO;
import com.swe.challenge.martin.LunchApplication.dto.response.SessionUuidResponseDTO;
import com.swe.challenge.martin.LunchApplication.mapper.LunchSessionMapper;
import com.swe.challenge.martin.LunchApplication.repository.LunchSessionDAO;
import com.swe.challenge.martin.LunchApplication.repository.LunchSessionUserDAO;
import com.swe.challenge.martin.LunchApplication.repository.RestaurantChoiceDAO;
import com.swe.challenge.martin.LunchApplication.repository.model.LunchSession;
import com.swe.challenge.martin.LunchApplication.repository.model.LunchSessionUser;
import com.swe.challenge.martin.LunchApplication.utils.MockUtils;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class LunchSessionServiceTest {

    @Mock
    private LunchSessionUserDAO lunchSessionUserDAO;

    @Mock
    private RestaurantChoiceDAO restaurantChoiceDAO;

    @Mock
    private LunchSessionDAO lunchSessionDAO;

    @InjectMocks
    private LunchSessionService lunchSessionService;

    @Mock
    private HttpServletResponse httpServletResponse;

    @Mock
    private HttpServletRequest httpServletRequest;


    @Test
    public void givenValidRequest_whenCreateSession_thenSuccess() {
        UsernameRequest req = MockUtils.createUsernameRequest();
        LunchSession lunchSession = MockUtils.createLunchSession();
        LunchSessionUser lunchSessionUser = MockUtils.createLunchSessionUser();

        when(lunchSessionDAO.saveAndFlush(any())).thenReturn(lunchSession);
        when(lunchSessionUserDAO.saveAndFlush(any())).thenReturn(lunchSessionUser);
        doNothing().when(httpServletResponse).addCookie(any());

        SessionUuidResponseDTO actual = lunchSessionService.createSession(httpServletResponse, req);
        SessionUuidResponseDTO expected = new SessionUuidResponseDTO();
        expected.setSessionId(lunchSession.getSessionId());
        expected.setSessionUuid(lunchSession.getSessionUuid());

        assertEquals(expected.getSessionUuid(), actual.getSessionUuid());
    }

    @Test
    public void givenValidRequest_whenGetSessionDetails_thenSuccess() {
        LunchSession expected = MockUtils.createLunchSession();
        when(lunchSessionDAO.findById(any())).thenReturn(Optional.ofNullable(expected));

        LunchSession actual = lunchSessionService.getSessionDetails(1L);
        assertEquals(expected.getSessionUuid(), actual.getSessionUuid());
    }

    @Test
    public void givenValidRequest_whenTerminateSession_thenSuccess() {
        LunchSession lunchSession = MockUtils.createLunchSession();

        Cookie[] cookies = MockUtils.createCookies();

        when(lunchSessionDAO.findById(any())).thenReturn(Optional.ofNullable(lunchSession));
        when(httpServletRequest.getCookies()).thenReturn(cookies);
        when(restaurantChoiceDAO.findRestaurantChoicesBySessionId(any())).thenReturn(Optional.of(new ArrayList<>()));
        when(lunchSessionDAO.save(any())).thenReturn(lunchSession);

        LunchSessionResponseDTO actual = lunchSessionService.terminateSession(httpServletRequest, 1L);
        lunchSession.setSessionStatus(SessionStatus.TERMINATED);

        LunchSessionResponseDTO expected = LunchSessionMapper.lunchSessionToDto(lunchSession);

        assertEquals(expected.getSessionStatus(), actual.getSessionStatus());
    }

    @Test
    public void givenValidRequest_whenJoinSession_thenSuccess() {
        UsernameRequest req = MockUtils.createUsernameRequest();
        LunchSession lunchSession = MockUtils.createLunchSession();
        Cookie[] cookies = MockUtils.createCookies();

        when(lunchSessionDAO.findById(any())).thenReturn(Optional.ofNullable(lunchSession));
        when(httpServletRequest.getCookies()).thenReturn(cookies);

        LunchSessionResponseDTO actual = lunchSessionService.joinSession(httpServletRequest, httpServletResponse,1L, req.getUsername());
        LunchSessionResponseDTO expected = LunchSessionMapper.lunchSessionToDto(lunchSession);

        assertEquals(expected.getSessionUuid(), actual.getSessionUuid());
    }
}
