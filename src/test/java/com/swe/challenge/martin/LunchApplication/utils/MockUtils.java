package com.swe.challenge.martin.LunchApplication.utils;

import com.swe.challenge.martin.LunchApplication.constants.SessionStatus;
import com.swe.challenge.martin.LunchApplication.dto.request.CreateRestaurantNameRequest;
import com.swe.challenge.martin.LunchApplication.dto.request.UpdateRestaurantNameRequest;
import com.swe.challenge.martin.LunchApplication.dto.request.UsernameRequest;
import com.swe.challenge.martin.LunchApplication.repository.model.LunchSession;
import com.swe.challenge.martin.LunchApplication.repository.model.LunchSessionUser;
import com.swe.challenge.martin.LunchApplication.repository.model.RestaurantChoice;
import jakarta.servlet.http.Cookie;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class MockUtils {

    public static UsernameRequest createUsernameRequest() {
        UsernameRequest req = new UsernameRequest();
        req.setUsername("username");
        return req;
    }

    public static LunchSession createLunchSession() {
        UsernameRequest req = createUsernameRequest();
        LocalDateTime now = LocalDateTime.now();
        return LunchSession.builder()
                .sessionId(1L)
                .sessionStatus(SessionStatus.STARTED)
                .createdAt(now)
                .sessionUuid("sessionUuid")
                .createdBy(req.getUsername())
                .build();
    }

    public static LunchSessionUser createLunchSessionUser() {
        UsernameRequest req = createUsernameRequest();
        LunchSession lunchSession = createLunchSession();
        return LunchSessionUser.builder()
                .lunchSession(lunchSession)
                .username(req.getUsername())
                .build();
    }

    public static Cookie createCookie() {
        UsernameRequest req = createUsernameRequest();
        LunchSession lunchSession = createLunchSession();
        String cookieValue = new StringBuilder()
                .append(CookieUtils.COOKIE_NAME_OWNER)
                .append("_")
                .append(StringUtils.transformUsernameForCookieValue(req.getUsername())).toString();
        Cookie userCookie = new Cookie(lunchSession.getSessionUuid(), cookieValue);
        return CookieUtils.setCookieParameters(userCookie);
    }

    public static Cookie[] createCookies() {
        return new Cookie[]{createCookie()};
    }

    public static RestaurantChoice createRestaurantChoice() {
        UsernameRequest req = createUsernameRequest();
        LunchSession lunchSession = createLunchSession();
        return RestaurantChoice.builder()
                .restaurantName("restaurantName")
                .createdBy(req.getUsername())
                .lunchSession(lunchSession)
                .build();
    }

    public static List<RestaurantChoice> createRestaurantChoices() {
        List<RestaurantChoice> restaurantChoices = new ArrayList<>();
        restaurantChoices.add(createRestaurantChoice());
        return restaurantChoices;
    }

    public static UpdateRestaurantNameRequest mockUpdateRestaurantNameRequest() {
        UsernameRequest usernameRequest = createUsernameRequest();
        UpdateRestaurantNameRequest updateRestaurantNameRequest = new UpdateRestaurantNameRequest();
        updateRestaurantNameRequest.setRestaurantName("updatedRestaurantName");
        updateRestaurantNameRequest.setUsername(usernameRequest.getUsername());

        return updateRestaurantNameRequest;
    }

    public static CreateRestaurantNameRequest mockCreateRestaurantNameRequest() {
        UsernameRequest usernameRequest = createUsernameRequest();
        CreateRestaurantNameRequest createRestaurantNameRequest = new CreateRestaurantNameRequest();
        createRestaurantNameRequest.setRestaurantName("restaurantName");
        createRestaurantNameRequest.setUsername(usernameRequest.getUsername());

        return createRestaurantNameRequest;

    }
}
