package com.swe.challenge.martin.LunchApplication.service;

import com.swe.challenge.martin.LunchApplication.dto.request.CreateRestaurantNameRequest;
import com.swe.challenge.martin.LunchApplication.dto.request.UpdateRestaurantNameRequest;
import com.swe.challenge.martin.LunchApplication.dto.request.UsernameRequest;
import com.swe.challenge.martin.LunchApplication.dto.response.RestaurantChoiceResponseDTO;
import com.swe.challenge.martin.LunchApplication.exceptions.constants.LunchApplicationException;
import com.swe.challenge.martin.LunchApplication.exceptions.constants.LunchApplicationExceptionMessage;
import com.swe.challenge.martin.LunchApplication.mapper.RestaurantChoiceMapper;
import com.swe.challenge.martin.LunchApplication.repository.LunchSessionDAO;
import com.swe.challenge.martin.LunchApplication.repository.LunchSessionUserDAO;
import com.swe.challenge.martin.LunchApplication.repository.RestaurantChoiceDAO;
import com.swe.challenge.martin.LunchApplication.repository.model.LunchSession;
import com.swe.challenge.martin.LunchApplication.repository.model.RestaurantChoice;
import com.swe.challenge.martin.LunchApplication.utils.MockUtils;
import com.swe.challenge.martin.LunchApplication.utils.RestaurantChoiceValidationUtils;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.transaction.Transactional;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class RestaurantChoiceServiceTest {

    @Mock
    private LunchSessionUserDAO lunchSessionUserDAO;

    @Mock
    private RestaurantChoiceDAO restaurantChoiceDAO;

    @Mock
    private LunchSessionDAO lunchSessionDAO;

    @InjectMocks
    private RestaurantChoiceService restaurantChoiceService;

    @Mock
    private HttpServletResponse httpServletResponse;

    @Mock
    private HttpServletRequest httpServletRequest;

    @Test
    public void givenValidRequest_whenGetRestaurantList_thenSuccess() {
        List<RestaurantChoice> restaurantChoices = MockUtils.createRestaurantChoices();
        when(restaurantChoiceDAO.findRestaurantChoicesBySessionId(any())).thenReturn(Optional.of(restaurantChoices));

        List<RestaurantChoiceResponseDTO> actual = restaurantChoiceService.getRestaurantList(1L);
        List<RestaurantChoiceResponseDTO> expected = restaurantChoices.stream().map(RestaurantChoiceMapper::RestaurantChoiceToDto).toList();

        for (int i = 0; i < expected.size(); i++) {
            assertEquals(expected.get(i).getCreatedBy(), actual.get(i).getCreatedBy());
            assertEquals(expected.get(i).getSessionId(), actual.get(i).getSessionId());
            assertEquals(expected.get(i).getRestaurantName(), actual.get(i).getRestaurantName());
        }

    }

    @Test
    public void givenValidRequest_whenGetRestaurantChoice_thenSuccess() {
        UsernameRequest req = MockUtils.createUsernameRequest();
        RestaurantChoice restaurantChoice = MockUtils.createRestaurantChoice();
        when(restaurantChoiceDAO.findRestaurantChoiceBySessionIdAndUsername(any(), any())).thenReturn(Optional.of(restaurantChoice));

        RestaurantChoiceResponseDTO actual = restaurantChoiceService.getRestaurantChoice(1L, req.getUsername());
        RestaurantChoiceResponseDTO expected = RestaurantChoiceMapper.RestaurantChoiceToDto(restaurantChoice);

        assertEquals(expected.getCreatedBy(), actual.getCreatedBy());
        assertEquals(expected.getSessionId(), actual.getSessionId());
        assertEquals(expected.getRestaurantName(), actual.getRestaurantName());
    }

    @Test
    public void givenValidRequest_whenUpdateRestaurantChoice_thenSuccess() {
        UpdateRestaurantNameRequest updateRestaurantNameRequest = MockUtils.mockUpdateRestaurantNameRequest();
        Cookie[] cookies = MockUtils.createCookies();
        RestaurantChoice restaurantChoice = MockUtils.createRestaurantChoice();
        restaurantChoice.setRestaurantName(updateRestaurantNameRequest.getRestaurantName());

        when(restaurantChoiceDAO.findById(any())).thenReturn(Optional.of(restaurantChoice));
        when(httpServletRequest.getCookies()).thenReturn(cookies);
        when(restaurantChoiceDAO.save(any())).thenReturn(restaurantChoice);

        RestaurantChoiceResponseDTO actual = restaurantChoiceService.updateRestaurantChoice(httpServletRequest, 1L, 1L, updateRestaurantNameRequest);
        RestaurantChoiceResponseDTO expected = RestaurantChoiceMapper.RestaurantChoiceToDto(restaurantChoice);

        assertEquals(expected.getCreatedBy(), actual.getCreatedBy());
        assertEquals(expected.getSessionId(), actual.getSessionId());
        assertEquals(expected.getRestaurantName(), actual.getRestaurantName());
    }

    @Test
    public void givenValidRequest_whenDeleteRestaurantChoice_thenSuccess() {
        Cookie[] cookies = MockUtils.createCookies();
        RestaurantChoice restaurantChoice = MockUtils.createRestaurantChoice();

        when(restaurantChoiceDAO.findById(any())).thenReturn(Optional.of(restaurantChoice));
        when(httpServletRequest.getCookies()).thenReturn(cookies);
        doNothing().when(restaurantChoiceDAO).delete(any());

        assertDoesNotThrow(() -> restaurantChoiceService.deleteRestaurantChoice(httpServletRequest, 1L, 1L));
        verify(restaurantChoiceDAO).delete(any());
    }

    @Test
    public void givenValidRequest_whenCreateRestaurantChoice_thenSuccess() {
        LunchSession lunchSession = MockUtils.createLunchSession();
        Cookie[] cookies = MockUtils.createCookies();
        RestaurantChoice restaurantChoice = MockUtils.createRestaurantChoice();
        CreateRestaurantNameRequest createRestaurantNameRequest = MockUtils.mockCreateRestaurantNameRequest();

        when(lunchSessionDAO.findById(any())).thenReturn(Optional.ofNullable(lunchSession));
        when(restaurantChoiceDAO.findRestaurantChoiceBySessionIdAndUsername(any(), any())).thenReturn(Optional.empty());
        when(restaurantChoiceDAO.save(any())).thenReturn(restaurantChoice);

        RestaurantChoiceResponseDTO actual = restaurantChoiceService.createRestaurantChoice(httpServletRequest, 1L, createRestaurantNameRequest);
        RestaurantChoiceResponseDTO expected = RestaurantChoiceMapper.RestaurantChoiceToDto(restaurantChoice);

        assertEquals(expected.getCreatedBy(), actual.getCreatedBy());
        assertEquals(expected.getSessionId(), actual.getSessionId());
        assertEquals(expected.getRestaurantName(), actual.getRestaurantName());

    }
}
